Description
==============

**Roilo, S., Engler, J. O., Václavík, T., & Cord, A. F. (2022). Landscape‐level heterogeneity of agri‐environment measures improves habitat suitability for farmland birds. Ecological Applications, e2720.
  Please acknowledge this paper when using the code or data outputs.


Multiscale_SDM.R
--------------
This R script is provided to facilitate the implementation of the multiscale SDM modelling framework described in Roilo et al. (in review).
Following inputs are required to run the SDMs:
  - bird dataset: shapefile holding georeferenced point data of different bird species;
  - rasters of environmental variables, e.g. elevation, slope, distance from highways, distance from forest edges, land cover maps for grassland, small woody features and urban areas;
  - IACS data: shapefiles holding spatially-explicit information on agricultural fields, e.g. grown crops, applied Agri-Environment Measures, for each year.
	       Each polygon in the dataset corresponds to an agricultural field, and the attribute table has the following columns: 
	        --"crop_code" = holds information on the type of crop grown on the field,
		--"Field_type" = specifies whether the field is an arable field or a premanent grassland field,
		--"EFA_TYP" = specifies the code of the Ecological Focus Area scheme applied on the field, if any,
		--"Organic" = indicates whether the field is organic or conventionally farmed (T/F), 
		--"AES_INFO1" = specifies the code of the Agri-Environment Scheme applied on the field, if any,
		--"AES_INFO2" = specifies the code of the second Agri-Environment Scheme applied on the field,
		--"geometry" = specifies the geometry of the polygon.
The code calls other functions saved in the R scripts "Multiscale_SDM_functions.R", and "Multiscale_SDM_runBIOMOD2_function.R"


Multiscale_SDM_functions.R
--------------
This R script contains useful functions for data filtering, variable selection, and calculation of environmental variables within circular windows 
of varying sizes.


Multiscale_SDM_runBIOMOD2_function.R
--------------
This R script is used for running the multiscale ensemble SDM models.


Uncertainty_maps.R
--------------
This R script produces uncertainty maps for the ensemble SDM models fitted with the previous R codes. The uncertainty maps are calculated as the standard deviation across the single 
algorithm projections from which the ensemble model is constructed.


Corresponding author: 
--------------
Stephanie Roilo **stephanie.roilo@tu-dresden.de**

Please get in touch if you have questions about the code or data.

For more information about the BESTMAP project, please see: www.bestmap.eu 


