###################################################
#
# Title: Uncertainty_maps
# Purpose: produce uncertainty maps for each modelled bird species by computing the standard deviation across single algorithm maps.
# Author: Stephanie Roilo, Technische Universit�t Dresden
# Date: last modified on 24 February 2022
#
###################################################

# set the language to EN
Sys.setenv(LANGUAGE="en")

## create uncertainty maps for each SDM  -----------------------
library(dplyr)
library(raster)
library(sf)
library(terra)
library(mapview)

# set working directory to model output folder to read the data
setwd("Model_outputs_path")

# set list of species� scientific name
spec_list = c( "Alauda arvensis", "Vanellus vanellus", "Carduelis cannabina", "Emberiza calandra",
               "Coturnix coturnix", "Motacilla flava", "Crex crex", "Anthus pratensis", "Charadrius dubius",
               "Saxicola rubicola", "Sylvia communis", "Emberiza citrinella", "Gallinago gallinago",
               "Lanius collurio","Saxicola rubetra")        
# set list of folder names where individual species� results were saved (NOTE that the order of IDs must match the order of species names above!)
ID_list = c("Skylark", "Lapwing", "CLinnet", "CornBunting", 
            "Quail", "Wagtail", "Corncrake", "MPipit", "Plover", 
            "27Apr_Stonechat", "Whitethroat", "Yellowhammer", "CSnipe",      
            "RBShrike","Whinchat")

for ( i in c(1:length(spec_list))) {
  ID = ID_list[i]
  species = spec_list[i]
  specdot = gsub(" ", ".", species)
  # set the path to the maps folder where the uncertainty map will be saved
  map.dir = paste(ID, "/maps/", sep="")
  # load the SDMs from the single algorithm full models - this is a rasterstack
  stackr = rast(paste0(specdot, "/proj_", ID, "/proj_", ID, "_", specdot, ".grd"))
  # calculate uncertainty as standard deviation across the different algorithms
  uncert = app(stackr, function(x) sd(x, na.rm=TRUE))
  # save to file
  writeRaster(uncert, filename= paste(map.dir, species, " current_SD.tif", sep=""), overwrite=TRUE)
  
  # do the same for the projection in the no AES scenario
  stackr2 = rast(paste0(specdot, "/proj_", ID, "_noAES/proj_", ID, "_noAES_", specdot, ".grd"))
  # calculate uncertainty as standard deviation across the different algorithms
  uncert2 = app(stackr2, function(x) sd(x, na.rm=TRUE))
  writeRaster(uncert2, filename= paste(map.dir, species, " no AES_SD.tif", sep=""), overwrite=TRUE)
}

rm(list=ls())

